/*
--- Day 4: Secure Container ---

You arrive at the Venus fuel depot only to discover it's protected by a password. The Elves had written the password on a sticky note, but someone threw it out.

However, they do remember a few key facts about the password:

    It is a six-digit number.
    The value is within the range given in your puzzle input.
    Two adjacent digits are the same (like 22 in 122345).
    Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).

Other than the range rule, the following are true:

    111111 meets these criteria (double 11, never decreases).
    223450 does not meet these criteria (decreasing pair of digits 50).
    123789 does not meet these criteria (no double).

How many different passwords within the range given in your puzzle input meet these criteria?
*/

fn main() {
    let digits = (264793_u32..=803935).map(|i| {
        [
            i / 100_000 % 10,
            (i / 10_000 % 10),
            (i / 1_000) % 10,
            (i / 100) % 10,
            (i / 10) % 10,
            i % 10,
        ]
    });

    println!(
        "a = {}",
        digits
            .clone()
            .filter(|digits| monotone_up(digits) && two_adjacent_elems_identical(digits))
            .count()
    );

    println!(
        "b = {}",
        digits
            .clone()
            .filter(|digits| monotone_up(digits)
                && two_adjacent_elems_identical_but_not_part_of_larger_run(digits))
            .count()
    );
}

/// An Elf just remembered one more important detail: the two adjacent matching digits are not part of a larger group of matching digits.
/// Given this additional criterion, but still ignoring the range rule, the following are now true:
/// 112233 meets these criteria because the digits never decrease and all repeated digits are exactly two digits long.
/// 123444 no longer meets the criteria (the repeated 44 is part of a larger group of 444).
/// 111122 meets the criteria (even though 1 is repeated more than twice, it still contains a double 22).
/// How many different passwords within the range given in your puzzle input meet all of the criteria?
#[inline]
fn monotone_up<T: PartialOrd>(p: &[T]) -> bool {
    Iterator::zip(p.iter(), p.iter().skip(1)).all(|(a, b)| a <= b)
}
#[inline]
fn two_adjacent_elems_identical<T: PartialEq>(p: &[T]) -> bool {
    Iterator::zip(p.iter(), p.iter().skip(1)).any(|(a, b)| a == b)
}

#[inline]
// this is ugly as hell. i'm sorry.
fn two_adjacent_elems_identical_but_not_part_of_larger_run<T: PartialEq>(p: &[T; 6]) -> bool {
    p[0] == p[1] && p[1] != p[2]
        || (p[0] != p[1] && p[1] == p[2] && p[2] != p[3])
        || (p[1] != p[2] && p[2] == p[3] && p[3] != p[4])
        || (p[2] != p[3] && p[3] == p[4] && p[4] != p[5])
        || (p[3] != p[4] && p[4] == p[5])
}
