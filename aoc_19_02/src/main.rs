use std::num::ParseIntError;
use std::str::FromStr;

fn main() {
    let input = parse(include_str!("input.txt")).unwrap();
    println!("a={}", a(input.clone(), 12, 2));

    if let Some((i, j)) = b(input) {
        println!("b={}", 100 * i + j)
    }
}

/// Once you have a working computer, the first step is to restore the gravity assist program (your puzzle input) to the "1202 program alarm" state it had just before the last computer caught fire.
/// To do this, before running the program, replace position 1 with the value 12 and replace position 2 with the value 2.
///  What value is left at position 0 after the program halts?
fn a(mut v: Vec<usize>, i: usize, j: usize) -> usize {
    v[1] = i;
    v[2] = j;
    run_program(v)[0]
}

fn b(v: Vec<usize>) -> Option<(usize, usize)> {
    const WANT: usize = 19690720;
    for i in 0..v.len() {
        for j in 0..v.len() {
            let got = a(v.clone(), i, j);
            if got == WANT {
                return Some((i, j));
            }
        }
    }
    None
}

fn run_program(mut v: Vec<usize>) -> Vec<usize> {
    const ADD: usize = 1;
    const MUL: usize = 2;
    const FINISHED: usize = 99;
    let mut i = 0;
    while i + 4 < v.len() {
        let (op, a, b, res) = (v[i], v[i + 1], v[i + 2], v[i + 3]);
        match op {
            ADD => v[res] = v[a] + v[b],
            MUL => v[res] = v[a] * v[b],
            FINISHED => return v,
            unknown => panic!("unknown upcode: {}", unknown),
        };
        i += 4;
    }
    v
}

fn parse(s: &str) -> Result<Vec<usize>, ParseIntError> {
    s.trim().split(',').map(usize::from_str).collect()
}

#[test]
fn test_parse() {
    assert_eq!(Ok(vec![1, 0, 0, 3]), parse("1,0,0,3"))
}

#[test]
fn test_run() {
    for (v, want) in vec![
        (vec![1, 0, 0, 0, 99], vec![2, 0, 0, 0, 99]),
        (vec![2, 3, 0, 3, 99], vec![2, 3, 0, 6, 99]),
        (vec![2, 4, 4, 5, 99, 0], vec![2, 4, 4, 5, 99, 9801]),
        (
            vec![1, 1, 1, 4, 99, 5, 6, 0, 99],
            vec![30, 1, 1, 4, 2, 5, 6, 0, 99],
        ),
    ] {
        assert_eq!(run_program(v), want)
    }
}
