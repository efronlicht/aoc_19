const INPUT: &str = include_str!("input.txt");

fn main() {
    let input = parse(INPUT).unwrap();
    println!("a={}", a(&input));
    println!("b={}", b(&input));
}

/// What is the sum of the fuel requirements for all of the modules on your spacecraft?
fn a(v: &[usize]) -> usize {
    v.iter().copied().map(fuel_naive).sum()
}
fn b(v: &[usize]) -> usize {
    v.iter().copied().map(fuel).sum()
}
/// Fuel itself requires fuel just like a module - take its mass, divide by three, round down, and subtract 2.
/// However, that fuel also requires fuel, and that fuel requires fuel, and so on. Any mass that would require negative fuel should instead be treated as if it requires zero fuel;
/// the remaining mass, if any, is instead handled by wishing really hard, which has no mass and is outside the scope of this calculation.
fn fuel(mut mass: usize) -> usize {
    let mut total = 0;
    while mass > 0 {
        mass = fuel_naive(mass);
        total += mass
    }
    total
}
/// to find the fuel required for a module, take its mass, divide by three, round down, and subtract 2.
fn fuel_naive(mass: usize) -> usize {
    (mass / 3).saturating_sub(2)
}

fn parse(s: &str) -> Result<Vec<usize>, std::num::ParseIntError> {
    s.lines().map(|line| line.trim().parse::<usize>()).collect()
}
