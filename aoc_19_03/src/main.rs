use std::collections::{BTreeMap, BTreeSet};
fn main() {
    let red = parse_moves(include_str!("input_red.txt")).expect("could not parse input_red.txt");
    let blue = parse_moves(include_str!("input_blue.txt")).expect("could not parse input_blue.txt");

    let (red_map, blue_map) = (touched(red.iter().copied()), touched(blue.iter().copied()));
    let intersection = BTreeSet::intersection(&red_map, &blue_map);
    println!(
        "a = {}",
        intersection
            .clone()
            .map(|(y, x)| y.abs() + x.abs()) // manhattan distance
            .min()
            .unwrap()
    );

    let (red_steps, blue_steps) = (dist_to_first_touch(red), dist_to_first_touch(blue));
    println!(
        "b = {}",
        intersection
            .map(|k| red_steps[k] + blue_steps[k])
            .min()
            .unwrap()
    )
}

fn parse_moves(s: &str) -> Option<Vec<Move>> {
    s.trim().split(',').map(parse_move).collect()
}
fn touched(wire: impl IntoIterator<Item = Move>) -> BTreeSet<(i32, i32)> {
    let mut touched = BTreeSet::new();
    let (mut y, mut x) = (0, 0);
    for Move(direction, dist) in wire {
        for _ in 0..dist {
            match direction {
                Direction::Up => y += 1,
                Direction::Down => y -= 1,
                Direction::Right => x += 1,
                Direction::Left => x -= 1,
            };
            touched.insert((y, x));
        }
    }
    touched
}

fn dist_to_first_touch(wire: impl IntoIterator<Item = Move>) -> BTreeMap<(i32, i32), usize> {
    let mut first_entered = BTreeMap::new();
    let (mut y, mut x) = (0, 0);
    let mut steps = 0;
    for Move(direction, dist) in wire {
        for _ in 0..dist {
            steps += 1;
            match direction {
                Direction::Up => y += 1,
                Direction::Down => y -= 1,
                Direction::Right => x += 1,
                Direction::Left => x -= 1,
            };
            first_entered.entry((y, x)).or_insert(steps);
        }
    }
    first_entered
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
struct Move(Direction, u16);

fn parse_move(s: &str) -> Option<Move> {
    let mut chars = s.chars();
    let direction = match chars.next()? {
        'R' => Direction::Right,
        'L' => Direction::Left,
        'D' => Direction::Down,
        'U' => Direction::Up,
        _ => return None,
    };
    Some(Move(
        direction,
        chars.collect::<String>().parse::<u16>().ok()?,
    ))
}
